const router = require('express').Router();
const asyncWrap = require('../utils/wrap');
const datesValidators = require('../models/date/validators');

const StatisticsController = require('../controllers/statistics');

router.get(
  '/',
  datesValidators.validateQuery(datesValidators.getStatisticsRequestFiltersSchema),
  asyncWrap(StatisticsController.makeStatistics),
);

module.exports = router;
