const router = require('express').Router();
const asyncWrap = require('../utils/wrap');
const datesValidators = require('../models/date/validators');

const DatesController = require('../controllers/dates');

router.post(
  '/',
  datesValidators.validatePostBody(datesValidators.postRequestSchema),
  asyncWrap(DatesController.create),
);
router.get('/:id', asyncWrap(DatesController.find));
router.get(
  '/users/:user',
  datesValidators.validateQuery(datesValidators.getRequestFiltersSchema),
  asyncWrap(DatesController.findByUser),
);
router.get(
  '/',
  datesValidators.validateQuery(datesValidators.getRequestFiltersSchema),
  asyncWrap(DatesController.findAll),
);
router.put(
  '/:id',
  datesValidators.validatePutBody(datesValidators.putRequestSchema),
  asyncWrap(DatesController.update),
);
router.delete('/:id', asyncWrap(DatesController.deleteOne));

module.exports = router;
