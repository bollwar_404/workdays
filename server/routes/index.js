const Dates = require('./dates');
const Statistics = require('./statistics');
const Users = require('./users');

module.exports = {
  Dates,
  Statistics,
  Users,
};
