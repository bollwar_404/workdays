const router = require('express').Router();
const asyncWrap = require('../utils/wrap');

const UsersController = require('../controllers/users');

router.post('/', asyncWrap(UsersController.create));
router.get('/:identifier', asyncWrap(UsersController.find));
router.get('/', asyncWrap(UsersController.findAll));
router.put('/:id', asyncWrap(UsersController.update));
router.delete('/:id', asyncWrap(UsersController.deleteUser));

module.exports = router;
