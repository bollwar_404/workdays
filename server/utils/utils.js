const moment = require('moment');

function makeDates(from, to) {
  const fromDate = moment(from, 'YYYY-MM-DD');
  const toDate = moment(to, 'YYYY-MM-DD');
  if (toDate.diff(fromDate, 'days') < 0) {
    const e = {
      code: 101,
      message: 'Invalid dates',
    };
    throw e;
  }
  if (toDate.diff(fromDate, 'days') === 0) {
    return [fromDate.format('YYYY-MM-DD')];
  }
  const result = [fromDate.format('YYYY-MM-DD')];
  while (toDate.diff(fromDate, 'days') !== 0) {
    fromDate.add(1, 'days');
    result.push(fromDate.format('YYYY-MM-DD'));
  }
  return result;
}

module.exports = {
  makeDates,
};
