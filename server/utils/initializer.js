const logger = require('./logger')('initializer');

const db = require('./db');
const models = require('../models');

function initAll() {
  logger.info('Start init all');
  db.init();
  models.init();
}

module.exports = {
  initAll,
};
