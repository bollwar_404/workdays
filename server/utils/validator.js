const Joi = require('joi');
const Promise = require('bluebird');

function isId(id) {
  return id.match(/[0-9a-fA-F]{24}$/);
}

const validate = Promise.promisify(Joi.validate, Joi);

module.exports = {
  isId,
  validate,
};
