const Logger = require('log4js');
const config = require('config');

function returnLogger(name) {
  const logger = Logger.getLogger(name);
  logger.level = config.logger.level;
  return logger;
}

module.exports = returnLogger;
