const mongoose = require('mongoose');
const logger = require('./logger')('db');
const config = require('config');

mongoose.Promise = global.Promise;

function init() {
  logger.info('Start init mongo connection');
  const connection = mongoose.connect(config.mongo.uri);

  connection
    .then((db) => {
      logger.info('Success connect to db');
      return db;
    })
    .catch((err) => {
      if (err.message.code === 'ETIMEDOUT') {
        logger.info('Attempting to re-establish database connection.');
        mongoose.connect();
      } else {
        logger.error('Error while attempting to connect to database:');
        logger.error(err);
      }
    });
}

module.exports = {
  init,
};
