const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compress = require('compression');
const config = require('config');
const uuid = require('uuid').v4;

const logger = require('./utils/logger')('server');
const initializer = require('./utils/initializer');

initializer.initAll();

const routes = require('./routes');

const app = express();
let gracefullyClosing = false;

function onShutDown(req, res, next) {
  if (gracefullyClosing) {
    res.set('Connection', 'close');
    res.status(503).end('Server is in the process of shutting down');
    return;
  }

  next();
}

function onIncomingRequest(req, res, next) {
  logger.info(`New ${req.method} request ${req.url}`);
  req.requestId = uuid();
  req.timeStart = process.hrtime();

  next();
}

function onSuccess(req, res, next) {
  if (!res.result && !res.statusCode) {
    next();
    return;
  }
  res.json({
    error: null,
    result: res.result,
  });
}

function onError(err, req, res, next) {
  try {
    const errString = JSON.stringify(err);
    logger.error(errString);
  } catch (e) {
    logger.error(e, err);
  }
  const code = err.status || 500;
  const error = {
    code: err.code || err.message.code || 100,
    message: err.message || 'INTERNAL_SERVER_ERROR',
  };

  res.status(code).json({
    error,
    result: null,
  });

  next();
}

app.enable('trust proxy');
app.use(cookieParser());
app.use(compress({}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(onShutDown);
app.use(onIncomingRequest);


app.use('/users', routes.Users);
app.use('/dates', routes.Dates);
app.use('/statistics', routes.Statistics);

app.use(onSuccess);
app.use(onError);

const httpServer = app.listen(config.server.port, config.server.host);
logger.info(`Server listening ${config.server.host}:${config.server.port}`);

process.on('SIGTERM', () => {
  gracefullyClosing = true;

  httpServer.close(() => {
    process.exit();
  });

  setTimeout(() => {
    process.exit(1);
  }, config.server.shutdownTimeout);
});
