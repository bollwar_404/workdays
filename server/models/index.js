const logger = require('../utils/logger')('modelsInit');

const DateModel = require('./date');
const UserModel = require('./user');

function init() {
  logger.info('Start init models');
  DateModel.init();
  UserModel.init();
}

module.exports = {
  init,
};
