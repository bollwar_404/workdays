const logger = require('../../utils/logger')('dateModel');
const mongoose = require('mongoose');
const moment = require('moment');
const schema = require('./schema');
const _ = require('lodash');

const TIME_LIMIT = 8;
const actionTypes = ['officeWork', 'remoteWork', 'hospital', 'holiday'];
const dateFormat = 'YYYY-MM-DD';

const DateSchema = new mongoose.Schema(schema);

async function findByKeys(params) {
  const searchParams = _.pick(params, ['date', 'user']);
  const dateObj = await this.findOne(searchParams);
  return dateObj;
}

function makeFilters(query) {
  const filter = {};
  if (!_.isEmpty(query)) {
    if (query.from) {
      filter.date = Object.assign(
        {},
        filter.date,
        { '$gte': moment(query.from).format(dateFormat) },
      );
    }
    if (query.to) {
      filter.date = Object.assign(
        {},
        filter.date,
        { '$lte': moment(query.to).format(dateFormat) },
      );
    }
    if (query.user) {
      filter.user = query.user;
    }
  }
  return filter;
}

DateSchema.pre('save', async function (next) {
  const time = this.actions.reduce((sum, action) => sum + action.time, 0);
  if (time > TIME_LIMIT) {
    const error = {
      code: 100,
      message: 'Time limit reached, max 8 hours',
    };
    next(error);
    return;
  }
  next();
});

DateSchema.statics.findByKeys = findByKeys;

DateSchema.statics.makeFilters = makeFilters;

DateSchema.statics.actionTypes = actionTypes;

DateSchema.statics.TIME_LIMIT = TIME_LIMIT;

function init() {
  logger.info('Start init date model');
  mongoose.model('date', DateSchema);
  logger.info('Init success');
}

module.exports = {
  actionTypes,
  init,
};
