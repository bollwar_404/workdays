const mongoose = require('mongoose');

module.exports = {
  date: {
    type: Date,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  actions: [{
    action: {
      type: String,
      required: true,
      enum: ['officeWork', 'remoteWork', 'hospital', 'holiday'],
    },
    time: {
      type: Number,
      required: true,
    },
  }],
};
