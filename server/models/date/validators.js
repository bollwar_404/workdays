const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const DateModel = require('./index');

const Joi = BaseJoi.extend(Extension);

const dateFormat = 'YYYY-MM-DD';
const iDRegex = /[0-9a-fA-F]{24}$/;

const actions = Joi.object({
  action: Joi.string().valid(DateModel.actionTypes).required(),
  time: Joi.number().required().max(8),
});

const date = Joi.date().format(dateFormat).optional().default();

const postRequestSchema = Joi.object({
  from: Joi.date().format(dateFormat).required(),
  to: Joi.date().format(dateFormat).required(),
  user: Joi.string().required(),
  actions: Joi.array().items(actions).required(),
});

const getRequestFiltersSchema = Joi.object({
  from: date,
  to: date,
});

const getStatisticsRequestFiltersSchema = Joi.object({
  from: date,
  to: date,
  user: Joi.string().regex(iDRegex).optional().default(),
});

const putRequestSchema = Joi.object({
  actions: Joi.array().items(actions).optional(),
});

function validateQuery(schema) {
  return (req, res, next) => {
    const query = Joi.validate(req.query, schema);
    if (query.error) {
      const error = {
        message: query.error.details[0].message,
      };
      res.status(400);
      next(error);
      return;
    }
    req.query = query.value;
    next();
  };
}

function validatePostBody(schema) {
  return (req, res, next) => {
    const validateResult = Joi.validate(req.body, schema);
    if (validateResult.error) {
      const error = {
        message: validateResult.error.details[0].message,
      };
      res.status(400);
      next(error);
      return;
    }
    req.body = validateResult.value;
    next();
  };
}

function validatePutBody(schema) {
  return (req, res, next) => {
    const validateResult = Joi.validate(req.body, schema);
    if (validateResult.error) {
      const error = {
        message: validateResult.error.details[0].message,
      };
      res.status(400);
      next(error);
      return;
    }
    req.body = validateResult.value;
    next();
  };
}

module.exports = {
  validateQuery,
  validatePutBody,
  validatePostBody,
  postRequestSchema,
  getRequestFiltersSchema,
  getStatisticsRequestFiltersSchema,
  putRequestSchema,
};
