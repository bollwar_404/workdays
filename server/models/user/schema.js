const mongoose = require('mongoose');
require('mongoose-type-email');

module.exports = {
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: mongoose.SchemaTypes.Email,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
  },
  phone: {
    type: Number,
  },
  description: {
    type: String,
  },
};
