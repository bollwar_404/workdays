const logger = require('../../utils/logger')('userModel');
const mongoose = require('mongoose');
const schema = require('./schema');

async function findByEmail(email) {
  const user = await this.findOne({ email });
  return user;
}

const UserSchema = new mongoose.Schema(schema);

UserSchema.statics.findByEmail = findByEmail;

function init() {
  logger.info('Start init user model');
  mongoose.model('user', UserSchema);
  logger.info('Init success');
}

module.exports = {
  init,
};
