const mongoose = require('mongoose');
const validator = require('../utils/validator');
const _ = require('lodash');

const User = mongoose.model('user');

async function create(req, res, next) {
  const user = new User(req.body);
  try {
    await user.save();
  } catch (e) {
    if (e.code && e.code === 11000) {
      e.status = 409;
    }
    throw e;
  }

  res.status(201);
  res.result = user;
  next();
}

async function find(req, res, next) {
  let user;
  try {
    if (validator.isId(req.params.identifier)) {
      user = await User.findById(req.params.identifier);
    } else {
      user = await User.findByEmail(req.params.identifier);
    }
  } catch (e) {
    throw e;
  }

  if (!user) {
    res.status(404);
  }
  res.result = user;
  next();
}

async function update(req, res, next) {
  let user;
  try {
    user = await User.findById(req.params.id);
    _.merge(user, req.body);
    await user.save();
  } catch (e) {
    if (e.code && e.code === 11000) {
      e.status = 409;
    }
    throw e;
  }
  res.status(204);
  res.result = 'success';
  next();
}

async function deleteUser(req, res, next) {
  try {
    await User.findByIdAndRemove(req.params.id);
  } catch (e) {
    throw e;
  }
  next();
}

async function findAll(req, res, next) {
  let result;
  try {
    result = await User.find();
  } catch (e) {
    throw e;
  }
  res.result = result;
  next();
}

module.exports = {
  create,
  find,
  update,
  deleteUser,
  findAll,
};
