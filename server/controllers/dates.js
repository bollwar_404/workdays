const mongoose = require('mongoose');
const Errors = require('../utils/errors');
const utils = require('../utils/utils');
const _ = require('lodash');

const DateModel = mongoose.model('date');

async function create(req, res, next) {
  const datesArr = utils.makeDates(req.body.from, req.body.to);
  const dates = [];
  while (datesArr.length) {
    const date = datesArr.pop();
    const dateModel = new DateModel(Object.assign({ date }, req.body));
    const existing = await DateModel.findByKeys(dateModel);
    if (existing) {
      const currentActions = dateModel.actions.concat(existing.actions);
      const summary = currentActions.reduce((sum, action) => sum + action.time, 0);
      if (summary > DateModel.TIME_LIMIT) {
        throw new Errors.DatesError(`Invalid actions time on date ${date}`);
      }
      existing.actions = currentActions;
      dates.push(existing);
    } else {
      dates.push(dateModel);
    }
  }
  await Promise.all(dates.map(d => d.save()));
  res.status(201);
  res.result = dates;
  next();
}

async function find(req, res, next) {
  let date;
  try {
    date = await DateModel.findById(req.params.id)
      .populate('user');
  } catch (e) {
    throw e;
  }

  if (!date) {
    res.status(404);
  }
  res.result = date;
  next();
}

async function findByUser(req, res, next) {
  req.filter.user = req.params.user;
  let dates;
  try {
    dates = await DateModel.find(DateModel.makeFilters(req.query)).populate('user');
  } catch (e) {
    throw e;
  }
  res.result = dates;
  next();
}

async function findAll(req, res, next) {
  let dates;
  try {
    dates = await DateModel.find(DateModel.makeFilters(req.query)).populate('user');
  } catch (e) {
    throw e;
  }
  res.result = _.sortBy(dates, ['date']);
  next();
}

async function update(req, res, next) {
  if (_.isEmpty(req.body)) {
    res.status(204);
    next();
  }
  const summary = req.body.actions.reduce((sum, action) => sum + action.time, 0);
  if (summary > DateModel.TIME_LIMIT) {
    throw new Errors.DatesError('Invalid actions time');
  }
  try {
    await DateModel.findByIdAndUpdate(req.params.id, req.body);
  } catch (e) {
    throw e;
  }
  res.status(204);
  next();
}

async function deleteOne(req, res, next) {
  try {
    await DateModel.findByIdAndRemove(req.params.id);
  } catch (e) {
    throw e;
  }
  res.status(204);
  next();
}

module.exports = {
  create,
  find,
  findByUser,
  findAll,
  update,
  deleteOne,
};
