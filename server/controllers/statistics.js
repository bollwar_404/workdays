const mongoose = require('mongoose');
const _ = require('lodash');

const DateModel = mongoose.model('date');

async function makeStatistics(req, res, next) {
  let dates;
  try {
    dates = await DateModel.find(DateModel.makeFilters(req.query));
  } catch (e) {
    throw e;
  }
  const result = {};
  const users = _(dates)
    .map('user')
    .uniq()
    .value();
  users.forEach((user) => {
    result[user] = {};
    DateModel.actionTypes.forEach((action) => {
      result[user][action] = 0;
    });
  });
  dates.forEach((date) => {
    date.actions.forEach((action) => {
      result[date.user][action.action] += action.time;
    });
  });
  res.result = result;
  next();
}

module.exports = {
  makeStatistics,
};
