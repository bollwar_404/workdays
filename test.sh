#!/bin/bash

docker-compose up -d
docker exec workdays_back_1 npm i -g mocha
docker exec workdays_back_1 mocha tests/api
docker-compose down