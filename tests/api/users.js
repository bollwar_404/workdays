/* eslint no-underscore-dangle:"off" */
/* global it, describe */
const chakram = require('chakram');
const config = require('config');

const host = `${config.server.host || 'localhost'}:${config.server.port}`;

describe('users', () => {
  const userParams = {
    name: 'Foma',
    email: 'foma@gm.com',
  };
  let user;
  it('Users post', async () => {
    const users = await chakram.post(`http://${host}/users/`, userParams);
    chakram.expect(users).to.have.status(201);
    chakram.expect(users).to.have.property('body');
    chakram.expect(users.body).to.have.property('result');
    chakram.expect(users.body.result).to.have.property('_id');
    user = users.body.result;
  });
  it('Should update user', async () => {
    const putUserResp = await chakram.put(`http://${host}/users/${user._id}`, { name: 'Ne' });
    chakram.expect(putUserResp).to.have.status(204);
  });
  it('Should return user', async () => {
    const getUserResp = await chakram.get(`http://${host}/users/${user._id}`);
    chakram.expect(getUserResp).to.have.status(200);
    chakram.expect(getUserResp).to.have.property('body');
    chakram.expect(getUserResp.body).to.have.property('result');
    chakram.expect(getUserResp.body.result).to.have.property('_id');
    chakram.expect(getUserResp.body.result._id).to.be.equal(user._id);
    chakram.expect(getUserResp.body.result.name).to.not.be.equal(user.name);
  });
  it('delete user', async () => {
    await chakram.delete(`http://${host}/users/${user._id}`);
  });
});
