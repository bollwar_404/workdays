/* eslint no-underscore-dangle:"off" */
/* global before, it, describe, after */
const chakram = require('chakram');
const config = require('config');

const host = `${config.server.host || 'localhost'}:${config.server.port}`;

describe('dates', () => {
  let user;
  let dates;
  const userParams = {
    name: 'Foma',
    email: 'foma@gm.com',
  };
  before(async () => {
    const users = await chakram.post(`http://${host}/users/`, userParams);
    user = users.body.result;
  });
  after(async () => {
    await chakram.delete(`http://${host}/users/${user._id}`);
  });
  it('Should save 3 dates', async () => {
    const postParams = {
      from: '2018-12-01',
      to: '2018-12-03',
      user: user._id,
      actions: [{
        action: 'holiday',
        time: 8,
      }],
    };
    const result = await chakram.post(`http://${host}/dates`, postParams);
    chakram.expect(result).to.have.status(201);
    chakram.expect(result).to.have.property('body');
    chakram.expect(result.body).to.have.property('result');
    chakram.expect(result.body.result.length).to.be.equal(3);
    dates = result.body.result;
  });
  it('Should update one', async () => {
    const updObj = {
      actions: [
        {
          action: 'holiday',
          time: 4,
        },
        {
          action: 'hospital',
          time: 4,
        },
      ],
    };
    const result = await chakram.put(`http://${host}/dates/${dates[0]._id}`, updObj);
    chakram.expect(result).to.have.status(204);
  });
  it('Should return date', async () => {
    const result = await chakram.get(`http://${host}/dates/${dates[0]._id}`);
    chakram.expect(result).to.have.status(200);
    chakram.expect(result).to.have.property('body');
    chakram.expect(result.body).to.have.property('result');
    chakram.expect(result.body.result).to.have.property('actions');
    chakram.expect(result.body.result.actions.length).to.be.equal(2);
  });
  it('Should return two dates', async () => {
    const result =
        await chakram.get(`http://${host}/dates/?from=2018-12-01&to=2018-12-02`);
    chakram.expect(result).to.have.status(200);
    chakram.expect(result).to.have.property('body');
    chakram.expect(result.body).to.have.property('result');
    chakram.expect(result.body.result.length).to.be.equal(2);
  });
  it('Delete dates', async () => {
    while (dates.length) {
      const date = dates.pop();
      await chakram.delete(`http://${host}/dates/${date._id}`);
    }
  });
});
