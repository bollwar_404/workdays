/* eslint no-underscore-dangle:"off" */
/* global before, it, describe, after */
const chakram = require('chakram');
const config = require('config');

const host = `${config.server.host || 'localhost'}:${config.server.port}`;

describe('statistics', () => {
  let user;
  let dates;
  const userParams = {
    name: 'Foma',
    email: 'foma@gm.com',
  };
  before(async () => {
    const users = await chakram.post(`http://${host}/users/`, userParams);
    user = users.body.result;
    const postParams = {
      from: '2018-12-01',
      to: '2018-12-03',
      user: user._id,
      actions: [{
        action: 'holiday',
        time: 8,
      }],
    };
    const result = await chakram.post(`http://${host}/dates`, postParams);
    dates = result.body.result;
  });
  after(async () => {
    await chakram.delete(`http://${host}/users/${user._id}`);
    while (dates.length) {
      const date = dates.pop();
      await chakram.delete(`http://${host}/dates/${date._id}`);
    }
  });
  it('Should return 1 user with 24 hours of holidays', async () => {
    const result = await chakram.get(`http://${host}/statistics/`);
    chakram.expect(result).to.have.status(200);
    chakram.expect(result).to.have.property('body');
    chakram.expect(result.body).to.have.property('result');
    chakram.expect(result.body.result).to.have.property(user._id);
    const statistics = result.body.result[user._id];
    chakram.expect(statistics.holiday).to.be.equal(24);
  });
  it('Should return 1 user with 16 hours of holidays', async () => {
    const result = await chakram.get(`http://${host}/statistics/?to=2018-12-02`);
    chakram.expect(result).to.have.status(200);
    chakram.expect(result).to.have.property('body');
    chakram.expect(result.body).to.have.property('result');
    chakram.expect(result.body.result).to.have.property(user._id);
    const statistics = result.body.result[user._id];
    chakram.expect(statistics.holiday).to.be.equal(16);
  });
});
