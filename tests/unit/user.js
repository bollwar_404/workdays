/* eslint no-underscore-dangle:"off" */
/* global before, it, describe, afterEach */
const { assert } = require('chai');
const mongoose = require('mongoose');
const sinon = require('sinon');
const model = require('../../server/models/user');

mongoose.Promise = global.Promise;

describe('models/user', () => {
  before(() => {
    model.init();
    this.User = mongoose.model('user');
  });

  afterEach(() => {
    sinon.sandbox.restore();
  });

  describe('name', () => {
    it('Should be required', () => {
      const user = new this.User();
      const error = user.validateSync();
      assert.equal(error.errors.name.message, 'Path `name` is required.');
    });

    it('Should success', () => {
      const user = new this.User({
        name: 'name',
      });
      const error = user.validateSync();
      assert.equal(error && error.errors.name, null);
    });
  });
  describe('email', () => {
    it('Should be required', () => {
      const user = new this.User();
      const error = user.validateSync();
      assert.equal(error.errors.email.message, 'Email address is required');
    });

    it('Should success', () => {
      const user = new this.User({
        email: 'a@b.c',
      });
      const error = user.validateSync();
      assert.equal(error && error.errors.email, null);
    });
  });
});
