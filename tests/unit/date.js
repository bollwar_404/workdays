/* eslint no-underscore-dangle:"off" */
/* global before, it, describe, afterEach */
const { assert } = require('chai');
const mongoose = require('mongoose');
const sinon = require('sinon');
const model = require('../../server/models/date');

mongoose.Promise = global.Promise;

describe('models/date', () => {
  before(() => {
    model.init();
    this.DateModel = mongoose.model('date');
  });

  afterEach(() => {
    sinon.sandbox.restore();
  });

  describe('date', () => {
    it('Should be required', () => {
      const date = new this.DateModel();
      const error = date.validateSync();
      assert.equal(error.errors.date.message, 'Path `date` is required.');
    });

    it('Should success', () => {
      const date = new this.DateModel({
        date: '2018-12-12',
      });
      const error = date.validateSync();
      assert.equal(error && error.errors.date, null);
    });

    it('Should be invalid', () => {
      const date = new this.DateModel({
        date: '2018-12-121',
      });
      const error = date.validateSync();
      assert.equal(error && error.errors.date.message, 'Cast to Date failed for value "2018-12-121" at path "date"');
    });
  });
  describe('user', () => {
    it('Should be required', () => {
      const date = new this.DateModel();
      const error = date.validateSync();
      assert.equal(error.errors.user.message, 'Path `user` is required.');
    });

    it('Should success', () => {
      const date = new this.DateModel({
        user: '5ab1486f1caee90027061a25',
      });
      const error = date.validateSync();
      assert.equal(error && error.errors.user, null);
    });

    it('Should be invalid', () => {
      const date = new this.DateModel({
        user: '5ab1486f1caee90027061a251',
      });
      const error = date.validateSync();
      assert.equal(error && error.errors.user.message, 'Cast to ObjectID failed for value "5ab1486f1caee90027061a251" at path "user"');
    });
  });
  describe('actions', () => {
    it('Should not be required', () => {
      const date = new this.DateModel();
      const error = date.validateSync();
      assert.equal(error.errors.actions, null);
    });

    it('Should success', () => {
      const date = new this.DateModel({
        actions: [
          {
            action: 'holiday',
            time: 8,
          },
        ],
      });
      const error = date.validateSync();
      assert.equal(error && error.errors.actions, null);
    });
  });
});
