FROM node:8.9.1
MAINTAINER Vlasov Pavel

ENV NODE_PATH $NODE_PATH:./node_modules:

ENV APP_HOME /opt/app

RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

ENV NODE_ENV docker-dev

RUN npm install -g nodemon

ADD package.json /tmp/package.json
RUN cd /tmp \
    && npm install \
    && cp -a /tmp/node_modules $APP_HOME \
    && rm -rf /tmp/node_modules

VOLUME .:$APP_HOME

EXPOSE 3000

CMD ["node", "index.js"]