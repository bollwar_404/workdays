# Workdays

Express-based application, that allows you to keep a calendar of employee working days.

## Installation

* `git clone` this repository
* `npm install`

## Running

* `npm start`

## Running with nodemon

* `npm run-script demon`

## Running Tests

* `npm test` - run simple unit tests.
* `npm run-script test-api` - run api tests with isolated DB, docker-compose required.

## Examples

* `POST http://localhost:3000/users`
``` js
{
    "name": "Foma",
    "email": "a@b.c"
}
```
Save user Foma with email a@b.c.

* `POST http://localhost:3000/dates`
``` js
{
    "from": "2018-12-12",
    "to": "2018-12-20",
    "user": "SOME_SAVED_USER_ID",
    "actions": [{
        "action": "holiday",
        "time": 8
    }]
}
```
Save information about the holidays of Foma
